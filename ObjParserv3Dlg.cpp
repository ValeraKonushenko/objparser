#include "pch.h"
#include "framework.h"
#include "ObjParserv3.h"
#include "ObjParserv3Dlg.h"
#include "afxdialogex.h"
#include "resource.h"
#include "Resource.h"



BEGIN_MESSAGE_MAP(CObjParserv3Dlg, CDialogEx)
	ON_WM_TIMER()
	ON_WM_SYSCOMMAND()
	
	ON_COMMAND_RANGE(IDC_RADIO1, IDC_RADIO3, &CObjParserv3Dlg::OnRadio)
	ON_COMMAND(IDC_LOAD, &CObjParserv3Dlg::OnLoad)
	ON_COMMAND(IDC_SAVE, &CObjParserv3Dlg::OnSave)
	ON_COMMAND(IDC_PARSE, &CObjParserv3Dlg::OnParse)
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()

CObjParserv3Dlg::CObjParserv3Dlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_OBJPARSERV3_DIALOG, pParent),
	parser(mut){
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	_message		= "";
	_message_color	= RGB(0, 0, 0);
}
#pragma warning(disable: 4996)

void CObjParserv3Dlg::SetInfTable(){
	SendMessage(L"Loading file...", 0);
	char* path = new char[PATHLEN];
	wcstombs(path, dlg_params.path_to_load, PATHLEN);
	char* buff = nullptr;
	if (!(buff = vku::getFileContent(path)))
		throw std::exception("File not loaded");

	SendMessage(L"Processing file...", 0);
	parser.ClearData();
	try {
		parser.ParseF(buff);

		const Parser::InnerData& idata = parser.GetData();

		_InfTable.amo_v.Format(L"%d", idata.f.size() * AMO_COMP_PER_VERT);
		_InfTable.amo_poly.Format(L"%d", idata.f.size());
		_InfTable.amo_vt.Format(L"%d", idata.f.size() * AMO_COMP_PER_VERT);
		_InfTable.amo_vn.Format(L"%d", idata.f.size() * AMO_COMP_PER_VERT);

		GetDlgItem(IDC_STATIC_AMO_VERT)->SetWindowTextW(_InfTable.amo_v);
		GetDlgItem(IDC_STATIC_AMO_POLY)->SetWindowTextW(_InfTable.amo_poly);
		GetDlgItem(IDC_STATIC_AMO_VERT_T)->SetWindowTextW(_InfTable.amo_vt);
		GetDlgItem(IDC_STATIC_AMO_VERT_N)->SetWindowTextW(_InfTable.amo_vn);
		SendMessage(L"Processing file have finished!", 0);
	}
	catch (const std::exception & ex) {
		this->SendMessage(CString(ex.what()), 1);
	}
	catch (...) {
		this->SendMessage(L"Has occured unidentified error...", 1);
	}
	delete[]path, buff;
}
void CObjParserv3Dlg::SetPathForSave(const CString& path){
	dlg_params.path_to_save = path;
	parser.SetPathTo(dlg_params.path_to_save);
	dlg_params.fname_for_save = "";
	GetDlgItem(IDC_STATIC_SAVE_PATH)->SetWindowTextW(dlg_params.path_to_save);
}
void CObjParserv3Dlg::SetPathForLoad(const CString& path, const CString& FileTitle){
	dlg_params.path_to_load = path;
	parser.SetPathFrom(dlg_params.path_to_load);
	dlg_params.fname_for_save = FileTitle;
	UpdateData(FALSE);
	GetDlgItem(IDC_FILENAME_EDIT)	->SetFocus();
	GetDlgItem(IDC_STATIC_LOAD_PATH)->SetWindowTextW(path);
}
void CObjParserv3Dlg::DoDataExchange(CDataExchange* pDX){
	CDialogEx::DoDataExchange(pDX);
	//DDX_Text(pDX, IDC_FNAME, dlg_params.fname);
	GetDlgItem(IDC_FNAME)->EnableWindow(dlg_params.is_active_fname);
	GetDlgItem(IDC_FILENAME_EDIT)->EnableWindow(dlg_params.is_active_fname);
	DDX_Text(pDX, IDC_FILENAME_EDIT, dlg_params.fname_for_save);
	DDX_Text(pDX, IDC_MSG_O, _message);
	DDX_Control(pDX, IDC_PROGRESS, progress_bar);
	progress_bar.SetRange(0, 100);
	this->UpdParam();	
}
BOOL CObjParserv3Dlg::OnInitDialog(){
	CDialogEx::OnInitDialog();
	SetIcon(m_hIcon, TRUE);	
	SetIcon(m_hIcon, FALSE);
	SetTimer(1000, 1, NULL);
	return TRUE;
}
void CObjParserv3Dlg::UpdParam(){
	CFont font[2];


	font[0].CreateFont(
		16,                        // nHeight
		0,                         // nWidth
		0,                         // nEscapement
		0,                         // nOrientation
		FW_NORMAL,                 // nWeight
		FALSE,                     // bItalic
		FALSE,                     // bUnderline
		0,                         // cStrikeOut
		ANSI_CHARSET,              // nCharSet
		OUT_DEFAULT_PRECIS,        // nOutPrecision
		CLIP_DEFAULT_PRECIS,       // nClipPrecision
		DEFAULT_QUALITY,           // nQuality
		DEFAULT_PITCH | FF_SWISS,  // nPitchAndFamily
		_T("Arial"));                 // lpszFacename
	GetDlgItem(IDC_FNAME)->SetFont(&font[0]);


	font[1].CreateFont(
		16,                        // nHeight
		0,                         // nWidth
		0,                         // nEscapement
		0,                         // nOrientation
		FW_NORMAL,                 // nWeight
		FALSE,                     // bItalic
		FALSE,                     // bUnderline
		0,                         // cStrikeOut
		ANSI_CHARSET,              // nCharSet
		OUT_DEFAULT_PRECIS,        // nOutPrecision
		CLIP_DEFAULT_PRECIS,       // nClipPrecision
		DEFAULT_QUALITY,           // nQuality
		DEFAULT_PITCH | FF_SWISS,  // nPitchAndFamily
		_T("Arial"));                 // lpszFacename
	GetDlgItem(IDC_MSG_O)->SetFont(&font[1]);
}

void CObjParserv3Dlg::OnLoad() {
	CFileDialog dlg(TRUE, NULL, L"*.obj");
	if (dlg.DoModal() == 1) {
		SetPathForLoad(dlg.GetPathName(), dlg.GetFileTitle());
		SetInfTable();

		if (dlg_params.path_to_save == CString(L"")) {
			CString tmp = dlg.GetPathName();
			int pos = tmp.ReverseFind('\\') + 1;
			tmp.Delete(pos, tmp.GetLength() - pos);
			SetPathForSave(tmp);
		}

	}
}
void CObjParserv3Dlg::OnSave(){
	CFolderPickerDialog dlg;
	if (dlg.DoModal() == 1) 
		SetPathForSave(dlg.GetPathName());
}
void CObjParserv3Dlg::OnParse(){
	UpdateData(TRUE);
	progress_bar.SetPos(0);
	SendMessage(L"", 0);
	Sleep(200);
	pr.is_done = false;
	pr.is_error = false;
	this->SendMessage(L"Start!", 0);
	std::thread th([&](){
		try
		{
			switch (dlg_params.style_type)
			{
			case ParseStyle::js_array:
			case ParseStyle::simple:
			case ParseStyle::js_obj: 
				if (pr.is_done = parser.Parse(dlg_params.style_type, dlg_params.path_to_save,
					dlg_params.fname_for_save))
					SetMessage(CString("File have parsed: ") + this->parser.GetPathTo(), 0);
				break;
			}
		}
		catch (const std::exception&ex)
		{
			SetMessage(CString(ex.what()), 1);
			pr.is_error = true;
			pr.is_done = true;
		}
	});	
	while (!pr.is_done) {
		float curr_per = parser.GetStagePercent();
		static float last_per = 0;
		if (last_per != curr_per) {
			CString str;
			str.Format(L"%.2f%%...", curr_per);
			this->SendMessage(str, 0);
			progress_bar.SetPos(curr_per);
			last_per = curr_per;
		}
	}
	progress_bar.SetPos(100);
	this->SendMessage();
	th.join();	
}
void CObjParserv3Dlg::OnRadio(UINT n){
	switch (n)
	{
	case IDC_RADIO1: {//Simple style
		SetSimpleStyle();
		break;
	}
	case IDC_RADIO2: {//Js array style
		SetJsArrayStyle();
		break;
	}
	case IDC_RADIO3: {//Js object style
		SetJsObjectStyle();
		break;
	}
	}
	UpdateData(TRUE);
}

void CObjParserv3Dlg::SetMessage(CString msg, bool is_bad){
	_message = msg;
	if (is_bad)	_message_color = RGB(255, 0, 0);
	else		_message_color = RGB(0, 171, 54);
}
void CObjParserv3Dlg::SendMessage() {
	UpdateData(FALSE);
}
void CObjParserv3Dlg::SendMessage(CString msg, bool is_bad){
	_message = msg;
	if (is_bad)	_message_color = RGB(255, 0, 0);
	else		_message_color = RGB(0, 171, 54);
	UpdateData(FALSE);
}

void CObjParserv3Dlg::SetSimpleStyle(){
	dlg_params.is_active_fname = true;
	dlg_params.fname = "Object name: ";
	dlg_params.style_type = ParseStyle::simple;
	((CButton*)GetDlgItem(IDC_RADIO1))->SetCheck(1);
	GetDlgItem(IDC_FNAME)->SetWindowTextW(dlg_params.fname);
}
void CObjParserv3Dlg::SetJsArrayStyle(){
	dlg_params.is_active_fname = true;
	dlg_params.fname = "Object name: ";
	dlg_params.style_type = ParseStyle::js_array;
	((CButton*)GetDlgItem(IDC_RADIO2))->SetCheck(1);
	GetDlgItem(IDC_FNAME)->SetWindowTextW(dlg_params.fname);
}
void CObjParserv3Dlg::SetJsObjectStyle(){
	dlg_params.is_active_fname = true;
	dlg_params.fname = "Object name: ";
	dlg_params.style_type = ParseStyle::js_obj;
	((CButton*)GetDlgItem(IDC_RADIO3))->SetCheck(1);
	GetDlgItem(IDC_FNAME)->SetWindowTextW(dlg_params.fname);
}

HBRUSH CObjParserv3Dlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	if (pWnd->GetDlgCtrlID() == IDC_MSG_O)
	{
		pDC->SetTextColor(_message_color);
	}
	return hbr;
}

void CObjParserv3Dlg::OnTimer(UINT_PTR id){
	static bool is_init = false;
	static bool last_is_new_onload = false;
	if (!is_init) {
		OnRadio(IDC_RADIO1);
		is_init = true;
	}
}
#pragma warning(default: 4996)
