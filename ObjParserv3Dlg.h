#pragma once
#include <atlevent.h>
#include "Parser.h"
#include <thread>
#include <mutex>

struct DlgParams {
	CString fname;
	bool is_active_fname;
	CString path_to_load;
	CString path_to_save;
	CString fname_for_save;
	ParseStyle style_type;
};
struct Process {
	bool is_done;
	bool is_error;
};
struct InfTable {
	CString amo_v;
	CString amo_poly;
	CString amo_vt;
	CString amo_vn;
};
class CObjParserv3Dlg : public CDialogEx {
private:
	InfTable _InfTable;
	Process pr;
	CString _message;
	COLORREF _message_color;
	std::mutex mut;
	CProgressCtrl progress_bar;
	void SetInfTable();
	void SetPathForSave(const CString& path);
	void SetPathForLoad(const CString& path, const CString& FileTitle);
protected:
	DECLARE_MESSAGE_MAP()
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	HICON m_hIcon; 
	virtual BOOL OnInitDialog();
	virtual void UpdParam();
	virtual void OnLoad();
	virtual void OnSave();
	virtual void OnParse();
	virtual void OnRadio(UINT n);
	DlgParams dlg_params;
	Parser_TS parser;
	void SetMessage(CString msg, bool is_bad);	//message has been set, but not displayed
	void SendMessage();							//message will sent to display
	void SendMessage(CString msg, bool is_bad);
	void SetSimpleStyle();
	void SetJsArrayStyle();
	void SetJsObjectStyle();
	HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
public:
	void OnTimer(UINT_PTR nIDEvent);
	CObjParserv3Dlg(CWnd* pParent = nullptr);
};