#pragma once
#include "framework.h"
#include <vector>
#include <string>
#include <mutex>
#include "vkutilslib.h"

#define OBJ_DOC_O  "o "		//rules by doc .obj
#define OBJ_DOC_V  "v "		//rules by doc .obj
#define OBJ_DOC_VT "vt "	//rules by doc .obj
#define OBJ_DOC_VN "vn "	//rules by doc .obj
#define OBJ_DOC_F  "f "		//rules by doc .obj
#define AMO_COMP_PER_VERT 3
class Parser_TS;
enum class ParseFlag :char {
	obj_to_jsarr_t
};
enum class ParseStyle {
	simple,
	js_array,
	js_obj
};
class Parser {
private:
	//AGRIGATION
	struct InData;
	class JsArrStyleParser {
	public:
		void ParseV(char* buff, InData & data);
		void ParseVT(char* buff, InData& data);
		void ParseVN(char* buff, InData& data);
	}_JsArrStyleParser;
	class JsObjStyleParser {
	public:
		void ParseV(char* buff, InData& data, CString objname);
		void ParseVT(char* buff, InData& data, CString objname);
		void ParseVN(char* buff, InData& data, CString objname);
	}_JsObjStyleParser;
	class SimpleStyleParser {
	public:
		void ParseV(char* buff, InData& data);
		void ParseVT(char* buff, InData& data);
		void ParseVN(char* buff, InData& data);
	}_SimpleStyleParser;

	class ProcessStage {
	private:
		float stage;
		float step;
	protected:
	public:
		void Reset(int amo_stage);
		float NextStep();
		ProcessStage(int amo_stage);
		float GetStagePercent();

	}_ProcessStage;

	struct FCompL2 {
		int v;
		int vt;
		int vn;
	};
	struct FCompL1 {
		FCompL2 c[AMO_COMP_PER_VERT];
	};
	struct InData {
		std::vector<std::string> v;
		std::vector<std::string> vt;
		std::vector<std::string> vn;
		std::vector<FCompL1> f;
	}data;
	
private:
	bool is_processing;
	const int amo_stage;
	size_t file_rstart;	//position for reading the object from file
	size_t file_rend;	//position for reading the object from file
	bool GetPosNextObject(char** rstart, char** rend);
protected:
	CString _pre_inf_serializing;
	CString _post_inf_serializing;
	CString path_from;
	CString path_to;
	float stage;
	size_t GetFileReadStart();
	size_t GetFileReadEnd();
	ParseStyle curr_parse_style;
public:
	void ClearData();
	typedef InData InnerData;
	const InData& GetData()const;
	void ParseF(char* buff);
	Parser();
	Parser(const Parser&)				= default;
	Parser& operator=(const Parser&)	= default;
	~Parser()							= default;
	virtual bool IsProcessing();
	bool IsSetPathFrom();
	bool IsSetPathTo();
	void SetPathFrom(CString path);
	void SetPathTo(CString path);
	virtual float GetStagePercent();
	CString GetPathFrom();
	CString GetPathTo();
	void Serialize(const CString& fname, ParseStyle ps);
	virtual bool Parse(ParseStyle ps, CString fname, 
		CString add_fname = CString(""));
	friend class Parser_TS;
};

class Parser_TS : public Parser {
protected:
	std::mutex &mut;
public:
	Parser_TS(std::mutex &mut);
	Parser_TS(const Parser_TS&) = default;
	Parser_TS& operator=(const Parser_TS&) = default;
	~Parser_TS() = default;
	virtual bool Parse(ParseStyle ps, CString fname, CString add_fname = CString(""));
	virtual float GetStagePercent();
};