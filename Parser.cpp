#include "pch.h"
#include "Parser.h"
#include <fstream>
#pragma warning(disable: 4996)



size_t Parser::GetFileReadStart(){
	return size_t(file_rstart);
}

size_t Parser::GetFileReadEnd(){
	return size_t(file_rend);
}

Parser::Parser():
	amo_stage(7),
	_ProcessStage(amo_stage){
	is_processing = false;
	file_rstart = 0u;
	file_rend = 0u;
}

const Parser::InData& Parser::GetData() const{
	return data;
}

void Parser::ParseF(char* buff) {
	if (buff == nullptr)
		throw std::exception("The string is empty");
	char* p = buff;	//pointer to faces
	char* pn = nullptr;		//pointer to \n
	char* tmp= nullptr;
	std::string fobj = OBJ_DOC_F;
	while (true) {
		p = strstr(p, fobj.c_str());
		if (!p)break;
		p += fobj.length();
		pn = strchr(p, '\n');
		int comp_counter = 0;
		FCompL1 fc;
		for (char* i = p; i < pn && comp_counter < AMO_COMP_PER_VERT; i++) {
			char* ps = strchr(i, ' ');
			if (ps == nullptr)
				ps = strchr(i, '\n');
			if (ps == nullptr)
				throw std::exception("Has occured some error with pointer..");
			if (ps > pn)ps = pn;
			int counter = 0;
			while (i < ps) {
				tmp = i;
				i = strpbrk(i, "0123456789");
				if (i > tmp + 1) {
					if (counter == 1)fc.c[comp_counter].vt = 0;
					else
						throw std::exception("Has occured some error with faces parsing");
				}
				else {
					if (counter == 0)fc.c[comp_counter].v = atoi(i);
					if (counter == 1)fc.c[comp_counter].vt = atoi(i);
					if (counter == 2)fc.c[comp_counter].vn = atoi(i);
					if (counter != 2)i = strchr(i, '/');
					else i = strchr(i, ' ');
				}
				if (!i)break;
				counter++;
			}
			comp_counter++;
		}
		data.f.push_back(fc);
	}
}
bool Parser::GetPosNextObject(char** rstart, char** rend){
	if (*rstart == nullptr && *rend == nullptr)
		throw std::exception("Has o�cured some error: GetPosNextObject");
	char* pos = strstr(*rstart + 1, OBJ_DOC_O);
	*rstart = pos;
	if (!pos) {
		*rstart = *rend = nullptr;
		return false;
	}
	pos = strstr((*rstart) + 1, OBJ_DOC_O);
	if (!pos) 
		*rend = strchr(*rstart, '\0');
	else
		*rend = pos;

	return true;
}
bool Parser::IsProcessing(){
	return is_processing;
}
bool Parser::IsSetPathFrom(){
	return !path_from.IsEmpty();
}
bool Parser::IsSetPathTo() {
	return !path_to.IsEmpty();
}
void Parser::SetPathFrom(CString path){
	this->path_from = path;
}
void Parser::SetPathTo(CString path){
	this->path_to = path;
}
float Parser::GetStagePercent(){
	return this->_ProcessStage.GetStagePercent();
}
CString Parser::GetPathFrom(){
	return CString(path_from);
}
CString Parser::GetPathTo() {
	return CString(path_to);
}
void Parser::Serialize(const CString &fname, ParseStyle ps){
	if (fname == "")
		throw std::exception("File name is empty or too short");
	using namespace std;
	fstream file;
	int cur_comp = 0;
	auto ser_vn = [&]() {
		CString path = path_to + "\\" + fname + "-vn.txt";
		cur_comp = 0;
		file.open(path.GetString(), ios::binary | ios::app);
		if (!file.is_open()) {
			file.open(path.GetString(), ios::binary | ios::out);
			file.close();
			file.open(path.GetString(), ios::binary | ios::app);
			if (!file.is_open())
				throw std::exception("Has ocurred some error with creating the file with vector-normals");
		}
		for (int i = 0; i < data.f.size(); i++)
			for (int j = 0; j < AMO_COMP_PER_VERT; j++) {
				cur_comp = data.f[i].c[j].vn - 1;
				file.write(data.vn[cur_comp].c_str(),
					data.vn[cur_comp].length() * sizeof(data.vn[i][0]));
				file.write("\n", 1);
			}

		file.close();
	};
	auto ser_vt = [&]() {
		CString path = path_to + "\\" + fname + "-vt.txt";
		cur_comp = 0;
		file.open(path.GetString(), ios::binary | ios::app);
		if (!file.is_open()) {
			file.open(path.GetString(), ios::binary | ios::out);
			file.close();
			file.open(path.GetString(), ios::binary | ios::app);
			if (!file.is_open())
				throw std::exception("Has ocurred some error with creating the file with vector-textures");
		}
		for (int i = 0; i < data.f.size(); i++)
			for (int j = 0; j < AMO_COMP_PER_VERT; j++) {
				cur_comp = data.f[i].c[j].vt - 1;
				if (cur_comp > 0) {
					file.write(data.vt[cur_comp].c_str(),
						data.vt[cur_comp].length() * sizeof(data.vt[i][0]));
				}
				else {
					switch (ps)
					{
					case ParseStyle::simple:
						file.write("0.0 0.0", 7);
						break;
					case ParseStyle::js_array:
						file.write("0.0,0.0,", 8);
						break;
					case ParseStyle::js_obj:
						break;
					default:
						break;
					}
				}
				file.write("\n", 1);
			}
		
		file.close();
	};
	auto ser_v =  [&]() {
		CString path = path_to + "\\" + fname + "-v.txt";
		cur_comp = 0;
		file.open(path.GetString(), ios::binary | ios::app);
		if (!file.is_open()) {
			file.open(path.GetString(), ios::binary | ios::out);
			file.close();
			file.open(path.GetString(), ios::binary | ios::app);
			if (!file.is_open())
				throw std::exception("Has ocurred some error with creating the file with vector");
		}
		for (int i = 0; i < data.f.size(); i++)
			for (int j = 0; j < AMO_COMP_PER_VERT; j++) {
				cur_comp = data.f[i].c[j].v - 1;
				file.write(data.v[cur_comp].c_str(),
					data.v[cur_comp].length() * sizeof(data.v[i][0]));
				file.write("\n", 1);
			}
		
		file.close();
	};

	auto ser_vn_js_type = [&](const CString& path) {
		cur_comp = 0;
		file.open(path.GetString(), ios::binary | ios::app);
		if (!file.is_open()) {
			file.open(path.GetString(), ios::binary | ios::out);
			file.close();
			file.open(path.GetString(), ios::binary | ios::app);
			if (!file.is_open())
				throw std::exception("Has ocurred some error with creating the file with vector-normals");
		}
		std::string pre("\tnormal: [\n");
		std::string post("\t],\n");
		file.write(pre.c_str(), pre.length());
		for (int i = 0; i < data.f.size(); i++)
			for (int j = 0; j < AMO_COMP_PER_VERT; j++) {
				cur_comp = data.f[i].c[j].vn - 1;
				file.write("\t\t", 2);
				file.write(data.vn[cur_comp].c_str(),
					data.vn[cur_comp].length() * sizeof(data.vn[0][0]));
				file.write("\n", 1);
			}
		file.write(post.c_str(), post.length());
		file.close();
	};
	auto ser_vt_js_type = [&](const CString& path) {
		cur_comp = 0;
		file.open(path.GetString(), ios::binary | ios::app);
		if (!file.is_open()) {
			file.open(path.GetString(), ios::binary | ios::out);
			file.close();
			file.open(path.GetString(), ios::binary | ios::app);
			if (!file.is_open())
				throw std::exception("Has ocurred some error with creating the file with vector-textures");
		}
		std::string pre("\tuv: [\n");
		std::string post("\t],\n");
		file.write(pre.c_str(), pre.length());
		for (int i = 0; i < data.f.size(); i++)
			for (int j = 0; j < AMO_COMP_PER_VERT; j++) {
				cur_comp = data.f[i].c[j].vt - 1;
				file.write("\t\t", 2);
				if (cur_comp > 0) {
					file.write(data.vt[cur_comp].c_str(),
						data.vt[cur_comp].length() * sizeof(data.vt[i][0]));
				}
				else {
					switch (ps){
					case ParseStyle::simple:	file.write("0.0 0.0", 7); break;
					case ParseStyle::js_array:	file.write("0.0,0.0,", 8);break;
					case ParseStyle::js_obj:	file.write("0.0,0.0,", 8);break;
					}
				}
				file.write("\n", 1);
			}

		file.write(post.c_str(), post.length());
		file.close();
	};
	auto ser_v_js_type = [&](const CString& path) {
		cur_comp = 0;
		file.open(path.GetString(), ios::binary | ios::app);
		if (!file.is_open()) {
			file.open(path.GetString(), ios::binary | ios::out);
			file.close();
			file.open(path.GetString(), ios::binary | ios::app);
			if (!file.is_open())
				throw std::exception("Has ocurred some error with creating the file with vector");
		}
		std::string pre("\tvertex: [\n");
		std::string post("\t],\n");
		file.write(pre.c_str(), pre.length());
		for (int i = 0; i < data.f.size(); i++)
			for (int j = 0; j < AMO_COMP_PER_VERT; j++) {
				cur_comp = data.f[i].c[j].v - 1;
				file.write("\t\t", 2);
				file.write(data.v[cur_comp].c_str(),
					data.v[cur_comp].length() * sizeof(data.v[0][0]));
				file.write("\n", 1);
			}
		file.write(post.c_str(), post.length());
		file.close();
	};
	auto set_pre_ser_inf = [&](const CString& path) {
		file.open(path.GetString(), ios::binary | ios::out);
		if (!file.is_open())
			throw std::exception("Has ocurred some error with creating the file with vector-textures");
		size_t len = _pre_inf_serializing.GetLength() + 1;
		char* buff = new char[len];
		wcstombs(buff, _pre_inf_serializing, len);
		buff[len - 1] = 0;
		file.write(buff, len - 1);
		file.close();
		file.close();
	};
	auto set_post_ser_inf = [&](const CString &path) {
		file.open(path.GetString(), ios::binary | ios::app);
		if (!file.is_open()) {
			file.open(path.GetString(), ios::binary | ios::out);
			file.close();
			file.open(path.GetString(), ios::binary | ios::app);
			if (!file.is_open())
				throw std::exception("Has ocurred some error with creating the file with vector-textures");
		}
		
		size_t len = _post_inf_serializing.GetLength() + 1;
		char* buff = new char[len];
		wcstombs(buff, _post_inf_serializing, len);
		buff[len - 1] = 0;		
		file.write(buff, len-1);
		file.close();
	};
	
	switch (curr_parse_style)
	{
	case ParseStyle::simple:
	case ParseStyle::js_array: {
		ser_vn();
		_ProcessStage.NextStep();
		ser_vt();
		_ProcessStage.NextStep();
		ser_v();
		_ProcessStage.NextStep();
		break;
	}
	case ParseStyle::js_obj: {
		CString path = path_to + "\\" + fname + ".js";
		set_pre_ser_inf(path);
		ser_vn_js_type(path);
		_ProcessStage.NextStep();
		ser_vt_js_type(path);
		_ProcessStage.NextStep();
		ser_v_js_type(path);
		_ProcessStage.NextStep();
		set_post_ser_inf(path);
		break;
	}
	default:
		throw std::exception("No such parse-type");
	}
}
bool Parser::Parse(ParseStyle ps, CString fname, CString add_fname){
	add_fname.Replace(L" ", L"");

	this->ClearData();
	curr_parse_style = ps;
	_ProcessStage.Reset(amo_stage);
	if (this->GetPathFrom() == CString(""))
		throw std::exception("Set path for loading");
	if (this->GetPathTo() == CString(""))
		throw std::exception("Set path for saving");


	char *path = new char[PATHLEN];
	wcstombs(path, this->GetPathFrom() , PATHLEN);
	char* buff = nullptr;
	if (!(buff = vku::getFileContent(path)))
		throw std::exception("File not loaded");


	this->ParseF(buff);
	_ProcessStage.NextStep();	

	switch (ps){
	case ParseStyle::simple:
		this->_SimpleStyleParser.ParseV(buff, data);
		_ProcessStage.NextStep();			
		this->_SimpleStyleParser.ParseVT(buff, data);
		_ProcessStage.NextStep();
		this->_SimpleStyleParser.ParseVN(buff, data);
		_ProcessStage.NextStep();
		break;
	case ParseStyle::js_array:
		this->_JsArrStyleParser.ParseV(buff, data);
		_ProcessStage.NextStep();
		this->_JsArrStyleParser.ParseVT(buff, data);
		_ProcessStage.NextStep();
		this->_JsArrStyleParser.ParseVN(buff, data);
		_ProcessStage.NextStep();
		break;
	case ParseStyle::js_obj:
		_pre_inf_serializing = "/*Valerii Koniushenko - Parser v3.0*/\n";
		_pre_inf_serializing += "var ";
		_pre_inf_serializing += add_fname;
		_pre_inf_serializing += "= {\n";
		this->_JsObjStyleParser.ParseV(buff, data, add_fname);
		_ProcessStage.NextStep();
		this->_JsObjStyleParser.ParseVT(buff, data, add_fname);
		_ProcessStage.NextStep();
		this->_JsObjStyleParser.ParseVN(buff, data, add_fname);
		_ProcessStage.NextStep();
		_post_inf_serializing = "\n};";
		break;
	default:
		throw std::exception("No such parse-style-type");
		break;
	}
	delete[]buff;

	this->Serialize(add_fname, ps);

	delete[]path;
	return true;
}

void Parser::ClearData(){
	this->data.f.clear();
	this->data.v.clear();
	this->data.vn.clear();
	this->data.vt.clear();
	
	this->data.f.reserve(1000);
	this->data.v.reserve(1000);
	this->data.vn.reserve(1000);
	this->data.vt.reserve(1000);
}


//~~~~~~~~~JsArrStyleParser~~~~~~~~~
void Parser::JsArrStyleParser::ParseV(char* buff, InData& data){
	if (buff == nullptr)
		throw std::exception("The string is empty");
	char* p = buff;			//pointer to vertex
	char* pn = nullptr;		//pointer to \n
	std::string fobj = OBJ_DOC_V;
	while (true) {
		p = strstr(p, fobj.c_str());
		if (!p)break;
		p += fobj.length();
		pn = strchr(p, '\n');
		std::string str(buff + (p - buff), pn - p);
		str[str.find_first_of(' ')] = ',';
		str[str.find_first_of(' ')] = ',';
		str += ',';
		data.v.push_back(str);
	}
}
void Parser::JsArrStyleParser::ParseVT(char* buff, InData& data){
	if (buff == nullptr)
		throw std::exception("The string is empty");
	char* p = buff;			//pointer to vertex-texure
	char* pn = nullptr;		//pointer to \n
	std::string fobj = OBJ_DOC_VT;
	while (true) {
		p = strstr(p, fobj.c_str());
		if (!p)break;
		p += fobj.length();
		pn = strchr(p, '\n');
		std::string str(buff + (p - buff), pn - p);
		str[str.find_first_of(' ')] = ',';
		str += ',';
		data.vt.push_back(str);
	}
}
void Parser::JsArrStyleParser::ParseVN(char* buff, InData& data){
	if (buff == nullptr)
		throw std::exception("The string is empty");
	char* p = buff;			//pointer to vertex-normal
	char* pn = nullptr;		//pointer to \n
	std::string fobj = OBJ_DOC_VN;

	while (true) {
		p = strstr(p, fobj.c_str());
		if (!p)break;
		p += fobj.length();
		pn = strchr(p, '\n');
		std::string str(buff + (p - buff), pn - p);
		str[str.find_first_of(' ')] = ',';
		str[str.find_first_of(' ')] = ',';
		str += ',';
		data.vn.push_back(str);
	}
}

//~~~~~~~~~JsObjStyleParser~~~~~~~~~
void Parser::JsObjStyleParser::ParseV(char* buff, InData& data, CString objname){
	if (buff == nullptr)
		throw std::exception("The string is empty");
	char* p = buff;			//pointer to vertex
	char* pn = nullptr;		//pointer to \n
	std::string fobj = OBJ_DOC_V;
	while (true) {
		p = strstr(p, fobj.c_str());
		if (!p)break;
		p += fobj.length();
		pn = strchr(p, '\n');
		std::string str(buff + (p - buff), pn - p);
		str[str.find_first_of(' ')] = ',';
		str[str.find_first_of(' ')] = ',';
		str += ',';
		data.v.push_back(str);
	}
}
void Parser::JsObjStyleParser::ParseVT(char* buff, InData& data, CString objname){
	if (buff == nullptr)
		throw std::exception("The string is empty");
	char* p = buff;			//pointer to vertex-texure
	char* pn = nullptr;		//pointer to \n
	std::string fobj = OBJ_DOC_VT;
	while (true) {
		p = strstr(p, fobj.c_str());
		if (!p)break;
		p += fobj.length();
		pn = strchr(p, '\n');
		std::string str(buff + (p - buff), pn - p);
		str[str.find_first_of(' ')] = ',';
		str += ',';
		data.vt.push_back(str);
	}
}
void Parser::JsObjStyleParser::ParseVN(char* buff, InData& data, CString objname){
	if (buff == nullptr)
		throw std::exception("The string is empty");
	char* p = buff;			//pointer to vertex-normal
	char* pn = nullptr;		//pointer to \n
	std::string fobj = OBJ_DOC_VN;
	while (true) {
		p = strstr(p, fobj.c_str());
		if (!p)break;
		p += fobj.length();
		pn = strchr(p, '\n');
		std::string str(buff + (p - buff), pn - p);
		str[str.find_first_of(' ')] = ',';
		str[str.find_first_of(' ')] = ',';
		str += ',';
		data.vn.push_back(str);
	}
}

//~~~~~~~~~SimpleStyleParser~~~~~~~~~
void Parser::SimpleStyleParser::ParseV(char* buff, InData& data){
	if (buff == nullptr)
		throw std::exception("The string is empty");
	char* p = buff;			//pointer to vertex
	char* pn = nullptr;		//pointer to \n
	std::string fobj = OBJ_DOC_V;
	while (true) {
		p = strstr(p, fobj.c_str());
		if (!p)break;
		p += fobj.length();
		pn = strchr(p, '\n');
		
		data.v.push_back(
			std::string(buff + (p - buff), pn - p)
		);
	}
}
void Parser::SimpleStyleParser::ParseVT(char* buff, InData& data){
	if (buff == nullptr)
		throw std::exception("The string is empty");
	char* p = buff;			//pointer to vertex-texure
	char* pn = nullptr;		//pointer to \n
	std::string fobj = OBJ_DOC_VT;
	while (true) {
		p = strstr(p, fobj.c_str());
		if (!p)break;
		p += fobj.length();
		pn = strchr(p, '\n');
		data.vt.push_back(
			std::string(buff + (p - buff), pn - p)
		);
	}
}
void Parser::SimpleStyleParser::ParseVN(char* buff, InData& data){
	if (buff == nullptr)
		throw std::exception("The string is empty");
	char* p = buff;			//pointer to vertex-normal
	char* pn = nullptr;		//pointer to \n
	std::string fobj = OBJ_DOC_VN;
	while (true) {
		p = strstr(p, fobj.c_str());
		if (!p)break;
		p += fobj.length();
		pn = strchr(p, '\n');
		data.vn.push_back(
			std::string(buff + (p - buff), pn - p)
		);
	}
}


//~~~~~~~~~~~~~~~~~~~ProcessStage~~~~~~~~~~~~~~~~~~~
Parser::ProcessStage::ProcessStage(int amo_stage){
	stage = 0.f;
	step = 1.f / amo_stage;
}
float Parser::ProcessStage::GetStagePercent(){
	return stage * 100.f;
}
void Parser::ProcessStage::Reset(int amo_stage){
	stage = 0.f;
	step = 1.f / amo_stage;
}
float Parser::ProcessStage::NextStep(){
	const float E = 0.001f;
	if (stage + step <= 1.f + E)
		stage += step;
	return GetStagePercent();
}
#pragma warning(default: 4996)


/*------------------PARSER_TS------------------*/
Parser_TS::Parser_TS(std::mutex &mut):mut(mut){}
bool Parser_TS::Parse(ParseStyle ps, CString fname, CString add_fname){
	while(add_fname.Replace(L" ", L""));
	
	this->ClearData();
	curr_parse_style = ps;
	mut.lock();
	is_processing = true;
	mut.unlock();
	char* path = nullptr;
	char* buff = nullptr;
	try
	{

		mut.lock();
		_ProcessStage.Reset(amo_stage);
		mut.unlock();
		if (this->GetPathFrom() == CString(""))
			throw std::exception("Set path for loading");
		if (this->GetPathTo() == CString(""))
			throw std::exception("Set path for saving");



		path = new char[PATHLEN];
		wcstombs(path, this->GetPathFrom(), PATHLEN);
		buff = nullptr;
		if (!(buff = vku::getFileContent(path)))
			throw std::exception("File not loaded");
		


		this->ParseF(buff);
		mut.lock();
		_ProcessStage.NextStep();
		mut.unlock();

		switch (ps) {
		case ParseStyle::simple:
			this->_SimpleStyleParser.ParseV(buff, data);
			mut.lock();
			_ProcessStage.NextStep();
			mut.unlock();
			this->_SimpleStyleParser.ParseVT(buff, data);
			mut.lock();
			_ProcessStage.NextStep();
			mut.unlock();
			this->_SimpleStyleParser.ParseVN(buff, data);
			mut.lock();
			_ProcessStage.NextStep();
			mut.unlock();
			break;
		case ParseStyle::js_array:
			this->_JsArrStyleParser.ParseV(buff, data);
			mut.lock();
			_ProcessStage.NextStep();
			mut.unlock();
			this->_JsArrStyleParser.ParseVT(buff, data);
			mut.lock();
			_ProcessStage.NextStep();
			mut.unlock();
			this->_JsArrStyleParser.ParseVN(buff, data);
			mut.lock();
			_ProcessStage.NextStep();
			mut.unlock();
			break;
		case ParseStyle::js_obj:
			_pre_inf_serializing = "/*Valerii Koniushenko - Parser v3.0*/\n";
			_pre_inf_serializing += "var ";
			_pre_inf_serializing += add_fname;
			_pre_inf_serializing += "= {\n";
			this->_JsObjStyleParser.ParseV(buff, data, add_fname);
			mut.lock();
			_ProcessStage.NextStep();
			mut.unlock();
			this->_JsObjStyleParser.ParseVT(buff, data, add_fname);
			mut.lock();
			_ProcessStage.NextStep();
			mut.unlock();
			this->_JsObjStyleParser.ParseVN(buff, data, add_fname);
			mut.lock();
			_ProcessStage.NextStep();
			mut.unlock();
			_post_inf_serializing = "\n};";
			break;
		default:
			throw std::exception("No such parse-style-type");
			break;
		}
		delete[]buff;
		this->Serialize(add_fname, ps);
		delete[]path;
	}
	catch (const std::exception & ex)
	{
		mut.lock();
		is_processing = false;
		mut.unlock();
		throw std::exception(ex.what());
	}
	mut.lock();
	is_processing = false;
	mut.unlock();	
	return true;
}

float Parser_TS::GetStagePercent(){
	mut.lock();
	float out = Parser::GetStagePercent();
	mut.unlock();
	return out;
}

