#include "pch.h"
#include "framework.h"
#include "ObjParserv3.h"
#include "ObjParserv3Dlg.h"

BEGIN_MESSAGE_MAP(CObjParserv3App, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


CObjParserv3App::CObjParserv3App(){}

CObjParserv3App theApp;

BOOL CObjParserv3App::InitInstance(){
	CWinApp::InitInstance();
	CShellManager *pShellManager = new CShellManager;
	CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerWindows));
	SetRegistryKey(_T("Local AppWizard-Generated Applications"));

	CObjParserv3Dlg dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK){
	}
	else if (nResponse == IDCANCEL){
	}
	else if (nResponse == -1)
	{
		TRACE(traceAppMsg, 0, "Warning: dialog creation failed, so application is terminating unexpectedly.\n");
		TRACE(traceAppMsg, 0, "Warning: if you are using MFC controls on the dialog, you cannot #define _AFX_NO_MFC_CONTROLS_IN_DIALOGS.\n");
	}

	if (pShellManager != nullptr)	{
		delete pShellManager;
	}

	return FALSE;
}

