#pragma once
#include "resource.h"

class CObjParserv3App : public CWinApp{
public:
	CObjParserv3App();
	virtual BOOL InitInstance();
	DECLARE_MESSAGE_MAP()
};

extern CObjParserv3App theApp;
