//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by ObjParserv3.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDS_ABOUTBOX                    101
#define IDD_OBJPARSERV3_DIALOG          102
#define IDR_MAINFRAME                   128
#define IDC_PROGRESS1                   1000
#define IDC_PROGRESS                    1000
#define IDC_LOAD                        1001
#define IDC_SAVE                        1002
#define IDC_FILENAME_EDIT               1003
#define IDC_FNAME                       1004
#define IDC_RADIO1                      1005
#define IDC_RADIO2                      1006
#define IDC_RADIO3                      1007
#define IDC_PARSE                       1008
#define IDC_FNAME2                      1009
#define IDC_MSG_O                       1009
#define IDC_STATIC_AMO_VERT             1011
#define IDC_STATIC_LOAD_PATH            1012
#define IDC_STATIC_AMO_POLY             1013
#define IDC_STATIC_AMO_VERT_T           1014
#define IDC_STATIC_AMO_VERT_VN          1015
#define IDC_STATIC_AMO_VERT_N           1015
#define IDC_STATIC_SAVE_PATH            1017

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1012
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
